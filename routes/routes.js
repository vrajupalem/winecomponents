const wineCompRoutes = require('./WineCompRoute');

const appRouter = (app,fs, jp) => {

    // default route that handles empty routes
    app.get('/', (req, res) => {
        res.send('Running default route of the WineComponent Service');
    });

    // Run the WineCompRoutes
    wineCompRoutes(app,fs, jp);

};

module.exports = appRouter;