const wineCompRoutes = (app, fs, jp) => {
    // Json file path 
    const jsonPath = './JSON-files/11YVCHAR001.json';

    const readFile = (callback, returnJson = false, filePath = jsonPath, encoding ='utf8') => {
        fs.readFile(filePath, encoding, (err, data)=> {
            if (err) {
                throw err;
            }

            callback(returnJson ? JSON.parse(data) : data);
        })
    }

    // Read JSON file and get wine components
    app.get('/wineComps', (req, res) => {
        fs.readFile(jsonPath, 'utf8', (err, data) => {
            if (err) {
                throw err;
            }
            
            var obj = JSON.parse(data);
            res.send(obj.components);
            console.log(obj.components);

        });
    });

    // Test read function 
    app.get('/read', (req, res) => {

        readFile(data => {
            res.send(data);
            console.log(data);
            console.log(data.lotCode);
            console.log(data.components);
        },
            true);
    });

    // Routes to develop
    // getYearBreakdown -- It needs to be ordered from highest percentage to lowest 
    // and we only want to see one line for each unique year
    app.get("/yearBreakDown", (req,res) => {

        readFile(data => {
           
           // sort from highest to lowest percentage
           var sorted = data.components.sort((a,b) => b.percentage - a.percentage);
    
            const yearRow = item => (item.percentage +'% - ' + item.year);
            const breakdown = sorted.map(yearRow);
            
            const brkStr = breakdown.join('\n');
            console.log(brkStr);
            res.send(brkStr);
        },
            true);
        
    });

    // getVarietyBreakdown
    app.get("/varietyBreakDown", (req,res) => {

        readFile(data => {
           
            // sort from highest to lowest percentage
            var sorted = data.components.sort((a,b) => b.percentage - a.percentage);
     
             const varietyRow = item => (item.percentage +'% - ' + item.variety);
             const breakdown = sorted.map(varietyRow);
             
             const brkStr = breakdown.join('\n');
             console.log(brkStr);
             res.send(brkStr);
         },
             true);
        
    });
    
    // getRegionBreakdown
    app.get("/regionBreakDown", (req,res) => {

        readFile(data => {
           
            // sort from highest to lowest percentage
            var sorted = data.components.sort((a,b) => b.percentage - a.percentage);
     
             const regionRow = item => (item.percentage +'% - ' + item.region);
             const breakdown = sorted.map(regionRow);
             
             const brkStr = breakdown.join('\n');
             console.log(brkStr);
             res.send(brkStr);
         },
             true);
        
    });
    // getYearAndVarietyBreakdown
    app.get("/yearAndVarietyBreakDown", (req,res) => {

        readFile(data => {
           
            // sort from highest to lowest percentage
            var sorted = data.components.sort((a,b) => b.percentage - a.percentage);
     
             const varietyRow = item => (item.percentage +'% - ' + item.variety + " - " + item.year);
             const breakdown = sorted.map(varietyRow);
             
             const brkStr = breakdown.join('\n');
             console.log(brkStr);
             res.send(brkStr);
         },
             true);
        
    });

    
};

module.exports = wineCompRoutes;