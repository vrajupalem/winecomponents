// Load Express and body parser
const express = require("express");
const bodyParser = require('body-parser');
const jp = require('jsonpath');

// create an instance of express to serve end points
const app =  express();

// load node's built in file system helper library to handle JSON files
const fs = require('fs');

// configure express instance with some body-parser settings 
// including handling JSON data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// this is where we'll handle our various routes from
const routes = require('./routes/routes.js')(app, fs, jp);

// Main express route of the application
app.get('/', (req, res) => {
    res.send("This is our main endpoint!");
})

const server = app.listen(4545, () => {
    console.log("Up and running ! -- This is our Wine Components Service");
    console.log('listening on port %s ... ', server.address().port);
})